﻿using System;
using System.Windows.Forms;

namespace Timestamper {
	class ObserverDateTimePicker : DateTimePicker, IObserver<DateTime> {
		private IDisposable unsubscriber;

		public virtual void Subscribe(IObservable<DateTime> provider) {
			if (provider != null)
				unsubscriber = provider.Subscribe(this);
		}

		public virtual void OnCompleted() {
			//Console.WriteLine("The Location Tracker has completed transmitting data to {0}.", this.Name);
			this.Unsubscribe();
		}

		public virtual void OnError(Exception e) {
			Console.WriteLine("Exception caught in ObserverDateTimePicker: {0}", e.Message);
		}

		public virtual void OnNext(DateTime value) {
			Value = value;
		}

		public virtual void Unsubscribe() {
			unsubscriber.Dispose();
		}
	}
}
