﻿using System;
using System.Collections.Generic;

namespace Timestamper {
	class ObservableTimestamp : IObservable<DateTime> {
		private List<IObserver<DateTime>> observers;
		private DateTime _subjectState;

		public DateTime Timestamp {
			get { return _subjectState; }
			set { SetTimestamp(value); }
		}

		public ObservableTimestamp() {
			_subjectState = DateTime.Now;
			observers = new List<IObserver<DateTime>>();
		}

		public ObservableTimestamp(DateTime value)
			: base() {
			_subjectState = value;
		}

		public ObservableTimestamp(long ticks)
			: base() {
			_subjectState = new DateTime(ticks);
		}

		// Gets or sets subject state
		public DateTime SubjectState {
			get { return _subjectState; }
			set { SetTimestamp(value); }
		}

		public void SetTimestamp(Nullable<DateTime> value) {
			if (!value.HasValue) {
				throw new NullReferenceException();
			}

			_subjectState = value.Value;
			foreach (var observer in observers) {
					observer.OnNext(value.Value);
			}
		}

		public IDisposable Subscribe(IObserver<DateTime> observer) {
			if (!observers.Contains(observer))
				observers.Add(observer);
			return new Unsubscriber(observers, observer);
		}

		private class Unsubscriber : IDisposable {
			private List<IObserver<DateTime>> _observers;
			private IObserver<DateTime> _observer;

			public Unsubscriber(List<IObserver<DateTime>> observers, IObserver<DateTime> observer) {
				this._observers = observers;
				this._observer = observer;
			}

			public void Dispose() {
				if (_observer != null && _observers.Contains(_observer))
					_observers.Remove(_observer);
			}
		}
	}
}
