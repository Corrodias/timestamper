﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Timestamper {
	class ObserverTextBox : TextBox, IObserver<DateTime> {
		private IDisposable unsubscriber;
		private Color originalColor;

		public ObserverTextBox()
			: base() {
			originalColor = ForeColor;
		}

		public virtual void Subscribe(IObservable<DateTime> provider) {
			if (provider != null)
				unsubscriber = provider.Subscribe(this);
		}

		public virtual void OnCompleted() {
			//Console.WriteLine("The Location Tracker has completed transmitting data to {0}.", this.Name);
			this.Unsubscribe();
		}

		public virtual void OnError(Exception e) {
			Console.WriteLine("Exception caught in ObserverTextBox: {0}", e.Message);
		}

		public virtual void OnNext(DateTime value) {
			Text = value.Ticks.ToString();
		}

		public virtual void Unsubscribe() {
			unsubscriber.Dispose();
		}

		public virtual void ResetColor() {
			ForeColor = originalColor;
		}
	}
}
