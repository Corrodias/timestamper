﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Timestamper {
	public partial class MainForm : Form {
		private ObservableTimestamp fileTime;
		private Boolean timestampIsValid;

		public Boolean TimestampIsValid {
			get { return timestampIsValid; }
			set {
				timestampIsValid = value;
				CheckValidity();
			}
		}

		public MainForm() {
			InitializeComponent();

			fileTime = new ObservableTimestamp();

			dateTimePicker1.Subscribe(fileTime);
			textBox1.Subscribe(fileTime);

			fileTime.SetTimestamp(DateTime.Now);
		}

		private void textBox1_TextChanged(object sender, EventArgs e) {
			try {
				fileTime.SetTimestamp(new DateTime(Convert.ToInt64(textBox1.Text)));
				TimestampIsValid = true;
			} catch (System.Exception) {
				TimestampIsValid = false;
			}
		}

		private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
			try {
				fileTime.SetTimestamp(dateTimePicker1.Value);
				TimestampIsValid = true;
			} catch (System.Exception) {
				TimestampIsValid = false;
			}
		}

		private void MainForm_DragEnter(object sender, DragEventArgs e) {
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		private void MainForm_DragDrop(object sender, DragEventArgs e) {
			Array a = (Array) e.Data.GetData(DataFormats.FileDrop);

			if (a != null) {
				// Extract string from first array element
				// (ignore all files except first if number of files are dropped).
				string s = a.GetValue(0).ToString();
				SetFile(s);
			}
		}

		private void openFileButton_Click(object sender, EventArgs e) {
			if (openFileDialog1.ShowDialog() == DialogResult.OK) {
				try {
					SetFile(openFileDialog1.FileName);
				} catch (Exception ex) {
					MessageBoxEx.Show(this, "Error: Could not read file from disk.\nOriginal error: " + ex.Message);
				}
			}
		}

		private void setButton_Click(object sender, EventArgs e) {
			WriteFileTimestamp();
		}

		private void CheckValidity() {
			SetTicksTextBoxColor();
			SetSetButtonStatus();
		}

		private void SetTicksTextBoxColor() {
			if (TimestampIsValid) {
				textBox1.ResetColor();
			} else {
				textBox1.ForeColor = Color.Red;
			}
		}

		private void SetSetButtonStatus() {
			if (TimestampIsValid && currentFileTextBox.Text.Length > 0) {
				setButton.Enabled = true;
			} else {
				setButton.Enabled = false;
			}
		}

		private void SetFile(string filepath) {
			DateTime fileModifiedDate = File.GetLastWriteTime(filepath);
			currentFileTextBox.Text = filepath;
			fileTime.SetTimestamp(fileModifiedDate);
		}

		private void WriteFileTimestamp() {
			try {
				File.SetLastWriteTime(currentFileTextBox.Text, fileTime.Timestamp);
				MessageBoxEx.Show(this, "File last modified time has been set.");
			} catch (Exception ex) {
				MessageBoxEx.Show(this, "Error: Could not write to file.\nOriginal error: " + ex.Message);
			}
		}
	}
}
